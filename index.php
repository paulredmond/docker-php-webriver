<?php

require __DIR__ . '/vendor/autoload.php';

use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\WebDriverBy;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\ProcessBuilder;

// Spin up the webdriver process
// $process = (new ProcessBuilder())
//     ->setPrefix('/usr/bin/chromedriver')
//     ->getProcess();

// $process->run(function ($type, $buffer) {
//     echo getenv("DISPLAY");
//     if (Process::ERR === $type) {
//         echo "Error > {$buffer}";
//     } else {
//         echo "Info > {$buffer}";
//     }
// });

// // executes after the command finishes
// if (!$process->isSuccessful()) {
//     throw new ProcessFailedException($process);
// }

$options = new ChromeOptions();
$prefs = array('download.default_directory' => '/tmp');
$options->setExperimentalOption('prefs', $prefs);
$capabilities = DesiredCapabilities::chrome(); 
$capabilities->setCapability(ChromeOptions::CAPABILITY, $options);

$driver = RemoteWebDriver::create(
    'http://127.0.0.1:9515', $capabilities, 5000
);

$driver->get('https://google.com');
echo $driver->getPageSource();
$driver->quit();
// $process->stop();
exit;
