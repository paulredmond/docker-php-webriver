#!/bin/bash

# Get the server number for xvfb
function get_server_num() {
  echo $(echo $DISPLAY | sed -r -e 's/([^:]+)?:([0-9]+)(\.[0-9]+)?/\2/')
}

# Prep xvfb
SERVERNUM=$(get_server_num)
rm -f /tmp/.X*lock

# Run xvfb
Xvfb :$SERVERNUM -screen 0 1024x768x16 &
NODE_PID=$!
echo "Xvfb on display $DISPLAY"

# Run chromedriver
/usr/bin/chromedriver &
CHROMEDRIVER_PID=$!

cleanup ()
{
  kill -s SIGTERM $!
  kill -s SIGTERM $NODE_PID
  kill -s SIGTERM $CHROMEDRIVER_PID
  wait $!
  wait $NODE_PID
  wait $CHROMEDRIVER_PID
  exit 0
}

trap cleanup SIGINT SIGTERM

# Run scheduler
while [ 1 ]
do
  sleep 60 &
  wait $!
done
